#include "register.h"
#include "gtest/gtest.h"

class DummyInterface : public Register::Interface {
public:
    uint8_t value = 0;
    uint8_t load(uint8_t address) override {
        return value;
    }
    void commit(uint8_t address, uint8_t raw) override {
        value = raw;
    }
};

TEST(RegisterTest, load) {
    DummyInterface interface;
    Register r = Register(0, 0, &interface);
    interface.value = 0xAA;
    EXPECT_EQ(r.get(), 0);
    r.load();
    EXPECT_EQ(r.get(), 0xAA);
}

TEST(RegisterTest, get_boolean) {
    DummyInterface interface;
    interface.value = 0xAA;
    Register r = Register(0, 0, &interface);
    r.load();

    EXPECT_FALSE(r.get(0));
    EXPECT_TRUE(r.get(1));
    EXPECT_FALSE(r.get(2));
    EXPECT_TRUE(r.get(3));
    EXPECT_FALSE(r.get(4));
    EXPECT_TRUE(r.get(5));
    EXPECT_FALSE(r.get(6));
    EXPECT_TRUE(r.get(7));
}

TEST(RegisterTest, get_int) {
    DummyInterface interface;
    interface.value = 0xAA;
    Register r = Register(0, 0, &interface);
    r.load();
    EXPECT_EQ(r.get(0, 8), 0xAA);
    EXPECT_EQ(r.get(1, 7), 0x55);
    EXPECT_EQ(r.get(0, 4), 0xA);
    EXPECT_EQ(r.get(4, 4), 0xA);
}

TEST(RegisterTest, get_enum) {
    enum MyEnum { e1, e2, e3, e4 };
    DummyInterface interface;
    interface.value = e3 << 4;
    Register r = Register(0, 0, &interface);
    r.load();
    EXPECT_EQ(r.get<MyEnum>(4, 4), e3);
}

TEST(RegisterTest, commit) {
    DummyInterface interface;
    Register r = Register(0, 0, &interface);
    r.set(0xAA);
    EXPECT_EQ(interface.value, 0);
    EXPECT_EQ(r.get(), 0xAA);
    r.commit();
    EXPECT_EQ(interface.value, 0xAA);
    EXPECT_EQ(r.get(), 0xAA);
}

TEST(RegisterTest, set_boolean) {
    DummyInterface interface;
    Register r{0, 0, &interface};
    for (int i = 0; i < 8; ++i) {
        r.set(true, i);
        EXPECT_EQ(interface.value, 0);
        EXPECT_EQ(r.get(), 1 << i);
        r.commit();
        EXPECT_EQ(interface.value, 1 << i);
        EXPECT_EQ(r.get(), 1 << i);
        r.set(0);
        r.commit();
    }
    for (int i = 0; i < 8; ++i) {
        r.set(0xFF);
        r.commit();
        EXPECT_EQ(interface.value, 0xFF);
        EXPECT_EQ(r.get(), 0xFF);
        r.set(false, i);
        EXPECT_EQ(interface.value, 0xFF);
        EXPECT_EQ(r.get(), ~(1 << i) & 0xFF);
        r.commit();
        EXPECT_EQ(interface.value, ~(1 << i) & 0xFF);
        EXPECT_EQ(r.get(), ~(1 << i) & 0xFF);
    }
}

TEST(RegisterTest, set_int) {
    DummyInterface interface;
    Register r{0, 0, &interface};
    EXPECT_EQ(interface.value, 0x00);
    EXPECT_EQ(r.get(), 0x00);
    r.set(0x5, 4, 4);
    r.commit();
    EXPECT_EQ(interface.value, 0x50);
    r.set(0x5, 0, 4);
    r.commit();
    EXPECT_EQ(interface.value, 0x55);
}