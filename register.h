#ifndef REGISTER_H
#define REGISTER_H

#include <array>
#include <cstddef>
#include <cstdint>

class Register {
public:
    struct Interface {
        virtual uint8_t load(uint8_t address) = 0;
        virtual void commit(uint8_t address, uint8_t raw) = 0;
    };

private:
    uint8_t mAddress;
    uint8_t mValue;
    Interface* mInterface;

public:
    explicit Register(uint8_t address, uint8_t raw, Interface* interface)
        : mAddress(address), mValue(raw), mInterface(interface) {}

    template<typename T> explicit Register(T address, uint8_t raw, Interface* interface) :
        mAddress(static_cast<uint8_t>(address)), mValue(raw), mInterface(interface) {}

    inline void set(uint8_t value) { mValue = value; }

    inline void set(uint8_t value, uint8_t shift, uint8_t size) {
        uint8_t mask = (0xFF >> (8 - size)) << shift;
        mValue = (mValue & ~mask) | ((value << shift) & mask);
    }

    template<typename T> inline void set(T value, uint8_t shift, uint8_t size) {
        set(static_cast<uint8_t>(value), shift, size);
    }

    inline void set(bool value, uint8_t shift) { set(value ? 1 : 0, shift, 1); }

    [[nodiscard]] inline uint8_t get() const { return mValue; }

    [[nodiscard]] inline uint8_t get(uint8_t shift, uint8_t size) const {
        uint8_t mask = 0xFF >> (8 - size);
        return (mValue >> shift) & mask;
    }

    template<typename T> [[nodiscard]] inline T get(uint8_t shift, uint8_t size) const {
        return static_cast<T>(get(shift, size));
    }

    [[nodiscard]] inline bool get(uint8_t shift) const { return get(shift, 1) != 0; }

    void load() { mValue = mInterface->load(mAddress); }

    void commit() { mInterface->commit(mAddress, mValue); }
};

struct ReadOnlyRegister {
private:
    ::Register reg;

public:
    explicit ReadOnlyRegister(uint8_t address, uint8_t raw, Register::Interface* interface) : reg(address,
                                                                                                  raw,
                                                                                                  interface) {}

    template<typename T>
    explicit ReadOnlyRegister(T address, uint8_t raw, Register::Interface* interface) : reg(address, raw, interface) {}

    [[nodiscard]] inline uint8_t get() const { return reg.get(); }

    [[nodiscard]] inline uint8_t get(uint8_t shift, uint8_t size) const { return reg.get(shift, size); }

    template<typename T> [[nodiscard]] inline T get(uint8_t shift, uint8_t size) const {
        return reg.get<T>(shift, size);
    }

    [[nodiscard]] inline bool get(uint8_t shift) const { return reg.get(shift); }

    void load() { reg.load(); }
};

template<size_t N> struct RegisterGroup {
    std::array <Register, N> registers;
    explicit RegisterGroup(std::array <Register, N> registers) : registers(registers) {}

    void load() {
        for (auto& r : registers) { r.load(); }
    }
    void commit() {
        for (const auto& r : registers) { r.commit(); }
    }
};

template<size_t N> struct ReadOnlyRegisterGroup {
    std::array <ReadOnlyRegister, N> registers;
    explicit ReadOnlyRegisterGroup(std::array <ReadOnlyRegister, N> registers) : registers(registers) {}

    void load() {
        for (auto& r : registers) { r.load(); }
    }
};

#endif